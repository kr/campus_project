package com.sakura.dev.config;

import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/**
 * Created by rc452 on 2017/5/21.
 */
@EnableRedisHttpSession
public class SessionConfig {
}
