package com.sakura.dev.service;

import com.sakura.dev.domain.CpStudent;

/**
 * Created by rc452 on 2017/5/22.
 */

public interface CpStudentService {
    void save(CpStudent cpStudent);
}
