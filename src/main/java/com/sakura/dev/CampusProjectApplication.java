package com.sakura.dev;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CampusProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(CampusProjectApplication.class, args);
	}
}
